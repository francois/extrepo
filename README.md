Extrepo packaging
=================

Debian-external repositories exist. They are not going away.

There are many valid reasons for having a second repository to install
software. For instance:

* You want to use a more recent version of a particular piece of
  software, and it is not in [backports](https://backports.debian.org)
* You want to use something on your Debian system that is not (yet)
  packaged for Debian (because it is very new, not production ready,
  complex to package, changes often, or all of the above)
* You wish to use some non-free software that Debian cannot distribute
  due to licensing restrictions

There may be more reasons.

Currently, most external repositories are configured by asking the user
to download an unsigned shell script and to run that as root. While most
such scripts are fairly simple and downloaded over https, they do
require that the user trust that the script has not been replaced by
something malicious, with no way for her to verify that.

While the ideal situation would be that the software is made available
in Debian proper, this is not always possible. This package tries to
remedy the situation, by:

- Providing a (curated and signed) index of available external
  repositories;
- Providing a package, shipped with Debian, that downloads and validates
  that index, allowing users to enable external repositories by simply
  running a single command.

This should allow such users to enable external repositories without
having to run an unsigned script as root.

If you maintain your own external repository, please see the
[extrepo-data](https://salsa.debian.org/extrepo-team/extrepo-data)
package.
